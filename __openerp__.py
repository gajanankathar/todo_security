{
    'name': 'To-Do Manage access rights and group rule',
    'description': 'Manage    your    personal    Tasks    with    this    module.',
    'author': 'Gajanan Kathar',
    'depends': ['mail'],
    'data' : [
              'views/todo_views.xml',
              'views/todo_actions.xml',
              'security/ir.model.access.csv',
              'security/todo_access_rule.xml'
              ],
    'application': True,
}